import argparse

# setup functions
import os
import json
from behavior_model.args import default_config
def setup(args):
    data_folder = os.path.abspath(args.data_folder)
    project_folder = os.path.abspath(args.project_folder)
    if args.len_pred is None:
        args.len_pred = args.len_traj

    os.mkdir(project_folder)
    config = default_config(project_folder, data_folder, args.len_traj, args.len_pred)
    with open(config['config'], 'w') as config_file:
        json.dump(config, config_file, indent=2)
    os.mkdir(config['log_dir'])

# database functions
import behavior_model.data.build_database_histogram as db_count
import behavior_model.data.build_sample_database as db_build
class db:
    def counts(args):
        db_count.main(args)

    def build(args):
        db_build.main(args)

# model functions
import cli_model as model

def main():
    parser = argparse.ArgumentParser(prog='maggotuba')
    subparsers = parser.add_subparsers()

    # Project setup interface
    parser_setup = subparsers.add_parser('setup')
    parser_setup.add_argument('data_folder', type=str)
    parser_setup.add_argument('project_folder', type=str)
    parser_setup.add_argument('--len_traj', default=20, type=int)
    parser_setup.add_argument('--len_pred', default=None, type=int)
    parser_setup.set_defaults(func=setup)

    # Database creation interface
    parser_db = subparsers.add_parser('db')
    parser_db.set_defaults(func=lambda args: parser_db.print_usage())

    subparsers_db = parser_db.add_subparsers()
    parser_db_counts = subparsers_db.add_parser('count')
    db_count.add_arguments(parser_db_counts)
    parser_db_counts.set_defaults(func=db.counts)

    parser_db_build = subparsers_db.add_parser('build')
    db_build.add_arguments(parser_db_build)
    parser_db_build.set_defaults(func=db.build)


    # Model interface
    parser_model = subparsers.add_parser('model')
    parser_model.set_defaults(func=lambda args: parser_model.print_usage())

    subparsers_model = parser_model.add_subparsers()
    parser_model_train = subparsers_model.add_parser('train')
    model.add_arguments_train(parser_model_train)
    parser_model_train.set_defaults(func=model.train)

    parser_model_eval = subparsers_model.add_parser('eval')
    model.add_arguments_eval(parser_model_eval)
    parser_model_eval.set_defaults(func=model.eval)

    parser_model_embed = subparsers_model.add_parser('embed')
    model.add_arguments_embed(parser_model_embed)
    parser_model_embed.set_defaults(func=model.embed)


    args = parser.parse_args()
    args.func(args)
