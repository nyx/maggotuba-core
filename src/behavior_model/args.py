import argparse
import yaml
import json
import os
import torch
import torch.nn as nn
import copy
from attrdict import AttrDict
from datetime import datetime

path_config_folder = os.getcwd()+'/config/'


def make_args(train=True):
    parser = argparse.ArgumentParser()
    config = default_train_config if train else default_eval_config
    for key, value in config().items():
        if isinstance(value, list):
            parser.add_argument(f'--{key}', type=type(value[0]), default=value, nargs='*')
        elif value is None:
            parser.add_argument(f'--{key}', default=None)
        else:
            parser.add_argument(f'--{key}', type=type(value), default=value)

    args_dict = vars(parser.parse_args())
    args_attrdict = AttrDict()
    for k, v in args_dict.items():
        if isinstance(v, bool):
            if k.startswith('no_'):
                args_attrdict[k[3:]] = not(v)
            else:
                args_attrdict[k] = v
        else:
            args_attrdict[k] = v

    args_dict = args_attrdict
    
    #if a config folder is given, default arguments are replaced by the config file's arguments
    if args_dict['config']:
        l = []
        path_config = os.path.join(path_config_folder, args_dict['config'])
        for filename in os.listdir(path_config):
            exp_dict = copy.deepcopy(args_dict)
            with open(os.path.join(path_config,filename), 'r') as stream:
                config_dict = yaml.safe_load(stream)
                for key, value in config_dict.items():
                    exp_dict[key] = value
            exp_dict['log_dir'] = build_exp_folder(exp_dict)
            l.append(exp_dict)
        return l
    else:
        if train:
            args_dict['log_dir'] = build_exp_folder(args_dict)
        else:
            args_dict['log_dir'] = args_dict['eval_saved_model']
            args_dict['exp_folder'] = args_dict['eval_saved_model']
        return [args_dict]
    
def build_exp_folder(args_dict):
    if args_dict['exp_folder'] is None:
        path_log = os.path.join(args_dict['log_dir'],name_exp(args_dict['exp_name'], args_dict['seed']))
        os.makedirs(path_log)
        os.mkdir(os.path.join(path_log,'visu'))
        torch.save(args_dict, path_log+'/params.pt')
        args_dict['exp_folder'] = path_log
        
    return args_dict['exp_folder']

def name_exp(name, seed):
    now = datetime.now()
    s = now.strftime('%Y-%m-%d_%H-%M-%S_')
    s += str(seed) + '_'
    s += name
    return s

def default_train_config():
    config = AttrDict()
    # General.
    config.seed = 100                                     # Seed for PyTorch's random number generator 
    config.exp_name = ''
    config.data_dir = '/Users/hugues/Documents/pasteur/data' #'/pasteur/projets/policy02/Larva-Screen/hugues'
    config.log_dir = os.path.join(os.getcwd(), '/runs/')
    config.exp_folder = None

    config.config = ''
    config.num_workers = 4
    # Data.
    config.n_features = 10                                # number of coordinates in the time series
    config.len_traj = 20                                  # maximum length of the input trajectory
    config.len_pred = 20                                  # is related to the maximum length of the output trajectory
                                                          # maximum length of the output = 2*len_pred + len_traj

    # Model.
    config.dim_latent = 10
    config.activation = 'relu'
    config.enc_filters = [128, 64, 32, 32, 32,  16]  # number of filters in each layer of the conv2d and the conv1d module of the encoder
    config.dec_filters = [128, 64, 32, 32, 32,  16]  # number of filters in each layer of the conv2d and the conv1d module of the decoder
    config.enc_kernel = [(5,1), (1,config.len_traj), (5,1), (1,config.len_traj), (5,1), (1,config.len_traj)]                           # filter size (segment_dim, time_dim)
    config.dec_kernel = [(1,config.len_traj), (5,1), (1,config.len_traj), (5,1), (1,config.len_traj), (5,1)]                         # filter size (segment_dim, time_dim)

    config.bias = False
    config.enc_type = 'conv1d'                            # can be either 'conv1d', in which case there are only 1D convolutions, 
                                                          # or 'conv2d-1d', in which case the network starts out with 2D convolutions before switching to 1D
    config.enc_depth = 4
    config.dec_type = 'convtranspose1d'
    config.dec_depth = 4
    config.init = 'kaiming'
    config.n_clusters = 2
    config.dim_reduc = 'UMAP'
    config.list_len_traj = [20]                   # Length of the input sequences. When multiple lengths are specified,
                                                  # the encoder has several tails which are aggregated in the latent space by a linear layer.
    config.list_len_pred = [20]            # Length of the past and future that are decoded, in addition to the present.
                                                  # Total decoded length is 2*len_pred + len_traj

    # coherence check
    assert(config.len_pred  == max(config.list_len_pred))
    assert(config.len_traj == max(config.list_len_traj))
    assert(all((config.len_traj-sub_length)%2 == 0 for sub_length in config.list_len_traj)) # checks whether the sub-series can be centered

    # Training.
    config.optim_iter = 5                         # number of epochs
    config.batch_size = 10
    config.lr = 0.005
    config.loss = 'MSE'
    config.cluster_penalty = None#'DEC'
    config.cluster_penalty_coef = 0.
    config.length_penalty_coef = 0.
    config.grad_clip = 100.0
    config.optimizer = 'adam'
    config.scheduler = 'plateau'
    config.target = {'past','present','future'}   # which segments are predicted from the time series
                                                  # the way the multi scale decoder is setup at the moment, it doesn't make sense to specify only one of {past, future}
    assert(('past' in config.target) == ('future' in config.target))
                                
    return config

def default_eval_config():
    config = AttrDict()
    config.eval_saved_model = os.getcwd()+'/saved_model/'
    config.data_dir = '/home/alexandre/workspace/larva_dataset_2022_03_07.hdf5' #'/pasteur/projets/policy02/Larva-Screen/hugues'
    config.subdir = 'post_training_eval'
    config.batch_dir = 'batches' #'/pasteur/homes/hvanhass/structured-temporal-convolution/batches'
    config.log_dir = os.getcwd()+'/training_log'
    config.config = ''
    config.exp_name = ''
    config.exp_folder = None

    config.seed = 100
    config.validation = True
    config.num_workers = 0
    
    config.rescale_at_runtime = True

    config.dim_reduc = 'UMAP'

    return config

def default_config(project_folder, data_folder, len_traj, len_pred):
    config = {}

    # General.
    config['project_dir'] = project_folder
    config['seed'] = 100                                     # Seed for PyTorch's random number generator 
    config['exp_name'] = ''
    config['data_dir'] = ''
    config['raw_data_dir'] = data_folder
    config['log_dir'] = os.path.join(project_folder, 'training_log')
    config['exp_folder'] = None

    config['config'] = os.path.join(project_folder, 'config.json')
    config['num_workers'] = 4

    # Data.
    config['n_features'] = 10                                # number of coordinates in the time series
    config['len_traj'] = len_traj                                  # maximum length of the input trajectory
    config['len_pred'] = len_pred                                  # is related to the maximum length of the output trajectory
                                                          # maximum length of the output = 2*len_pred + len_traj

    # Model.
    config['dim_latent'] = 10
    config['activation'] = 'relu'
    config['enc_filters'] = [128, 64, 32, 32, 32,  16]  # number of filters in each layer of the conv2d and the conv1d module of the encoder
    config['dec_filters'] = [128, 64, 32, 32, 32,  16]  # number of filters in each layer of the conv2d and the conv1d module of the decoder
    config['enc_kernel'] = [(5,1), (1,config['len_traj']), (5,1), (1,config['len_traj']), (5,1), (1,config['len_traj'])]                           # filter size (segment_dim, time_dim)
    config['dec_kernel'] = [(1,config['len_traj']), (5,1), (1,config['len_traj']), (5,1), (1,config['len_traj']), (5,1)]                         # filter size (segment_dim, time_dim)

    config['bias'] = False
    config['enc_depth'] = 4
    config['dec_depth'] = 4
    config['init'] = 'kaiming'
    config['n_clusters'] = 2
    config['dim_reduc'] = 'UMAP'


    # Training.
    config['optim_iter'] = 1000                         # number of epochs
    config['pseudo_epoch'] = 100
    config['batch_size'] = 128
    config['lr'] = 0.005
    config['loss'] = 'MSE'
    config['cluster_penalty'] = None#'DEC'
    config['cluster_penalty_coef'] = 0.
    config['length_penalty_coef'] = 0.
    config['grad_clip'] = 100.0
    config['optimizer'] = 'adam'
    config['target'] = ['past','present','future']   # which segments are predicted from the time series
                                               # the way the multi scale decoder is setup at the moment, it doesn't make sense to specify only one of {past, future}
                                
    return config