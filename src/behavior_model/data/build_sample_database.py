import multiprocessing as mp
import os.path
import numpy.random as rnd
import numpy as np
import argparse
import h5py
import itertools
from datetime import datetime
import tqdm

from behavior_model.data.enums import Label, Timeslot, Tracker, Feature
from behavior_model.data.utils import center_coordinates, rotate

import json
# ################################################################################
# #                                                                              #
# #                            HARDCODED REJECTION SAMPLING                      #
# #                                                                              #
# ################################################################################

# #TODO Refactor to avoid hardcoded values

# # sample count obtained using exhaustive_sample_counting.py
# # hardcoded for simplicity
# # dimensions : {t5, t15} x {run, bend, stop, hunch, back, roll} x {setup, before, during, after}
# N_SAMPLES_NOT_SCALED = np.array([[[ 52131648,  23265657,   2624668, 303889498],
#                                   [ 18587317,   9011929,   2650373, 228331523],
#                                   [  1069264,    763524,    469436,  34341526],
#                                   [    23975,     34445,    733270,   5577181],
#                                   [   502655,    327770,    727195,  13387502],
#                                   [    51263,     26229,      6565,    430490]],

#                                  [[ 16790227,  19938756,   3023452, 172225718],
#                                   [  5541839,   7453778,   2641581,  90876749],
#                                   [    66120,    168406,    160658,   6385043],
#                                   [    19237,     28588,     51414,    346758],
#                                   [    53589,    100456,     77792,   2046120],
#                                   [     4553,      6255,     21254,    296754]]])

# N_SAMPLES_SCALED = np.array([[[ 44265378,  23287124,   2626831,  99076911],
#                               [ 16087105,   9017810,   2653400, 104674121],
#                               [   983756,    763512,    469672,  19415447],
#                               [    20444,     34455,    734542,   1924362],
#                               [   441881,    328837,    727931,   8786846],
#                               [    43568,     26233,      6575,    168208]],

#                              [[ 16777787,  19962812,   3023203, 113832211],
#                               [  5492955,   7459428,   2639699,  60492007],
#                               [    66324,    168762,    160765,   4181698],
#                               [    18907,     28604,     51429,    224825],
#                               [    53597,    100659,     77818,   1524450],
#                               [     4528,      6253,     21240,    205598]]])


# N_SAMPLES = N_SAMPLES_SCALED


# TARGET_SAMPLES = 100000               # to create enough samples use TARGET_SAMPLES = 100000
# TOTAL  = np.sum(N_SAMPLES)

# def compute_selection_rate(kw, TARGET_SAMPLES):
#     PROPORTIONS = N_SAMPLES/np.sum(N_SAMPLES)

#     TARGET_PROPORTIONS = N_SAMPLES.copy()
#     # do not sample from the 'setup' timeslot
#     TARGET_PROPORTIONS[:,:,Timeslot.SETUP.value] = 0.0
#     # do not sample rolls from t5
#     TARGET_PROPORTIONS[Tracker.T5.value,Label.ROLL.value,:] = 0.0
#     # normalize behavior-wise
#     TARGET_PROPORTIONS = TARGET_PROPORTIONS/np.sum(np.sum(TARGET_PROPORTIONS, axis=0 , keepdims=True), axis=2, keepdims=True)
#     # behavior weights
#     if kw == 'balanced':
#         BEHAVIOR_WEIGHTS = np.array([0.25, 0.25, 0.125, 0.125, 0.125, 0.125]).reshape(1,6,1)
#     if kw == 'run-bend':
#         BEHAVIOR_WEIGHTS = np.array([0.5, 0.5, 0., 0., 0., 0.]).reshape(1,6,1)

#     # allocate weights to behaviors
#     TARGET_PROPORTIONS = BEHAVIOR_WEIGHTS*TARGET_PROPORTIONS

#     if TARGET_SAMPLES == 'max':
#         SELECTION_PROPORTION = int(1./np.min(TARGET_PROPORTIONS/PROPORTIONS))
#     else:
#         SELECTION_PROPORTION = TARGET_SAMPLES/TOTAL
#     SELECTION_RATE = SELECTION_PROPORTION*TARGET_PROPORTIONS/PROPORTIONS
#     return SELECTION_RATE

# SELECTION_RATE = compute_selection_rate('balanced', TARGET_SAMPLES)
# assert(np.all(SELECTION_RATE <= 1.0))

# ###############################################################################
# #
# #              END OF HARDCODED REJECTION SAMPLING
# #
# ################################################################################

def compute_selection_rate(counts, target_samples, kind='balanced'):
    proportions = counts/np.sum(counts)

    target_proportions = counts.copy()
    # do not sample from the 'setup' timeslot
    target_proportions[:,:,Timeslot.SETUP.value] = 0.0
    # do not sample rolls from t5
    target_proportions[Tracker.T5.value,Label.ROLL.value,:] = 0.0
    # normalize behavior-wise
    target_proportions = target_proportions/np.sum(np.sum(target_proportions, axis=0 , keepdims=True), axis=2, keepdims=True)
    # behavior weights
    if kind == 'balanced':
        behavior_weights = np.array([0.25, 0.25, 0.125, 0.125, 0.125, 0.125]).reshape(1,6,1)
    if kind == 'run-bend':
        behavior_weights = np.array([0.5, 0.5, 0., 0., 0., 0.]).reshape(1,6,1)

    # allocate weights to behaviors
    target_proportions = behavior_weights*target_proportions

    if target_samples == 'max':
        selection_proportions = int(1./np.min(target_proportions/proportions))
    else:
        selection_proportions = target_samples/np.sum(counts)
    selection_rate = selection_proportions*target_proportions/proportions
    return selection_rate

def writer_process_target(filename, queue, len_traj, len_pred, target_samples):

    f = h5py.File(filename, 'w')
    group = f.create_group('samples')
    group.attrs['len_pred'] = len_pred
    group.attrs['len_traj'] = len_traj
    group.attrs['registered'] = 1
    group.attrs['rescaled'] = 'prestimulus'
    sample_idx = 0
    pbar = tqdm.tqdm(total=target_samples)
    while (to_write := queue.get()) is not None:
        data, tracker, line, protocol, datetime, larva_number, filename, timeslot, start_point, behavior, path = to_write
        dset = f.create_dataset(f'samples/sample_{sample_idx}', data.shape, dtype=data.dtype)
        dset[...] = data
        dset.attrs['tracker']      = tracker
        dset.attrs['line']         = line
        dset.attrs['protocol']     = protocol
        dset.attrs['datetime']     = datetime
        dset.attrs['larva_number'] = larva_number
        dset.attrs['filename']     = filename
        dset.attrs['timeslot']     = timeslot
        dset.attrs['start_point']  = start_point
        dset.attrs['behavior']     = behavior
        dset.attrs['path']         = path

        sample_idx += 1
        pbar.update()
    group.attrs['n_samples'] = sample_idx
    f.close()

def compute_rotation_matrix(data):
    '''Construct the appropriate rotation matrix to realign the data
       Uses the vector linking the tail to the head as a surrogate for body direction

       Parameters:
           data : a single sample of shape T * (2*n_trackpoints)
    '''
    mean_coordinates = np.mean(data, axis=0)

    p1 = mean_coordinates[:2]  # head
    p2 = mean_coordinates[-2:] # tail

    direction = p1-p2
    unit_vector = direction/np.linalg.norm(direction)
    cos, sin = unit_vector
    matrix = np.array([[cos,sin],
                      [-sin,cos]])
    return matrix

def center_coordinates(sample, indices):
    n_segments = (Feature.LAST_COORD.value - Feature.FIRST_COORD.value + 1)//2
    bias_x = np.mean(sample[indices,Feature.X_MID_SEGMENT.value])
    bias_y = np.mean(sample[indices,Feature.Y_MID_SEGMENT.value])
    sample[:,Feature.FIRST_COORD.value:] = sample[:,Feature.FIRST_COORD.value:] - np.tile([bias_x,bias_y], n_segments).reshape(1,-1)
    return sample

def filereader_process_target(input_queue, output_queue, len_traj, len_pred, selection_rates):
    rng = rnd.default_rng()
    while (fn := input_queue.get()) is not None:
        tracker = Tracker.from_path(fn)
        data = np.loadtxt(fn)

        # parse filename
        dirname, filename = os.path.split(fn)
        dirname, datetime = os.path.split(dirname)
        dirname, protocol = os.path.split(dirname)
        dirname, line     = os.path.split(dirname)
        larva_number = int((os.path.splitext(filename)[0]).split('_')[-1])

        # Normalize length according to mean length before stimulus
        after_settled_before_activation = np.logical_and(data[:, Feature.TIME.value]<Tracker.get_stimulus_time(tracker),
                                                        data[:, Feature.TIME.value]>Tracker.get_start_time(tracker))
        if np.sum(after_settled_before_activation) == 0:
            # No prestimulus data, the file is discarded
            continue
        mean_larva_length = np.mean(data[after_settled_before_activation, Feature.LENGTH.value])
        data[:, Feature.LENGTH.value:Feature.LAST_COORD.value+1] = data[:, Feature.LENGTH.value:Feature.LAST_COORD.value+1]/mean_larva_length

        # iterate across all windows
        window_length = 2*len_pred+len_traj
        midpoint = window_length//2-1 if window_length %2 else (window_length-1)//2-1

        try:
            for start_point in range(len(data)-window_length+1):
                sample = data[start_point:start_point+window_length]
                label = np.argmax(sample[midpoint, Feature.FIRST_LABEL.value:Feature.LAST_LABEL.value+1]).item()
                center_time = sample[midpoint,Feature.TIME.value]
                timeslot = Timeslot.from_timestamp(center_time, tracker)

                # perform sample rejection to balance the dataset
                if rng.random() > selection_rates[tracker.value, label, timeslot.value]:
                    continue

                # rotate the sample based on present values
                sample = sample.copy()
                sample = rotate(sample, slice(len_pred, len_pred+len_traj))

                # center the samples based on present values
                sample = center_coordinates(sample, slice(len_pred, len_pred+len_traj))

                output_queue.put((sample, tracker.name, line, protocol, datetime, larva_number, filename, timeslot.name, start_point, Label(label).name, fn))

        except ValueError as e:
            if str(e) == 'ValueError: window shape cannot be larger than input array shape':
                print('Not enough points to form a window')
            else:
                raise e

import re

def is_date_time(s):
    return re.match(r'^[0-9]{8}_[0-9]{6}$', s) is not None

def iterfiles(root):
    for tracker in ['t5', 't15']:
        trackerdir = os.path.join(root, tracker)
        if not os.path.isdir(trackerdir): continue
        for genotype in os.listdir(trackerdir):
            genotypedir = os.path.join(trackerdir, genotype)
            if not os.path.isdir(genotypedir): continue
            for protocol in os.listdir(genotypedir):
                protocoldir = os.path.join(genotypedir, protocol)
                if not os.path.isdir(protocoldir): continue
                for datetime in os.listdir(protocoldir):
                    datadir = os.path.join(protocoldir, datetime)
                    if not (is_date_time(datetime) and os.path.isdir(datadir)): continue
                    for fn in os.listdir(datadir):
                        if fn.endswith('.txt') and fn.startswith('Point_dynamics_'):
                            yield os.path.join(datadir, fn)

def main(args):
    # load counts
    try:
        counts = np.load(args.counts_file)
    except FileNotFoundError:
        raise FileNotFoundError("Please run 'maggotuba db counts' beforehand.")

    selection_rates = compute_selection_rate(counts, args.n_samples, kind='balanced')
    assert(np.all(selection_rates <= 1.0))

    # load config
    try:
        with open('config.json', 'r') as config_file:
            config = json.load(config_file)
    except FileNotFoundError:
        raise ValueError("Please provide a target directory or run from a project folder.")

    if args.len_traj is None:
        args.len_traj = config['len_traj']
    if args.len_pred is None:
        args.len_pred = config['len_pred']

    # build source from config file
    if args.source is None:
        args.source = config['raw_data_dir']
    args.source = os.path.abspath(args.source)

    # build target from config file
    if args.target is None:
        if config == None:
            try:
                with open('config.json', 'r') as config_file:
                    config = json.load(config_file)
            except FileNotFoundError:
                raise ValueError("Please provide a target directory or run from a project folder.")
        date = datetime.now().strftime('%Y_%m_%d')
        filename = f'larva_dataset_{date}_{args.len_traj}_{args.len_pred}_{args.n_samples}.hdf5'
        filepath = os.path.join(config['project_dir'], filename)
        args.target = filepath
    args.target = os.path.abspath(args.target)

    # set database file in config to target
    with open('config.json', 'r') as config_file:
        config = json.load(config_file)
    config['data_dir'] = args.target
    with open('config.json', 'w') as config_file:
        config = json.dump(config, config_file)

    input_queue = mp.Queue()
    output_queue = mp.Queue()

    pool = [mp.Process(target=filereader_process_target, args=(input_queue, output_queue,args.len_traj,args.len_pred,selection_rates))
            for _ in range(args.n_workers)]
    for p in pool:
        p.start()

    writer_process = mp.Process(target=writer_process_target, args=(args.target, output_queue,args.len_traj,args.len_pred, args.n_samples))
    writer_process.start()

    for fn in iterfiles(args.source):
        input_queue.put(fn)
    for p in pool:
        input_queue.put(None) # send termination flag
    for p in pool:
        p.join()

    output_queue.put(None) # send termination flag
    writer_process.join()

    # print stats about the dataset
    with h5py.File(args.target, 'r') as f:
        counts = 6*[0]
        mapping = {'RUN':0, 'BEND':1, 'STOP':2, 'HUNCH':3, 'BACK':4, 'ROLL':5}
        for sample in f['samples']:
            counts[mapping[f['samples'][sample].attrs['behavior']]] += 1
        total = sum(counts)
        print('Total : ', total)
        print(' '.join([f'{k.lower()} : {c/total*100:.1f}' for k, c in zip(mapping, counts)]))

def add_arguments(parser):
    parser.add_argument('--n_workers', default=40, type=int)
    parser.add_argument('--source', default=None, type=str)
    parser.add_argument('--target', default=None)
    parser.add_argument('--len_traj', default=None, type=int)
    parser.add_argument('--len_pred', default=None, type=int)
        # TODO change this to load config

    parser.add_argument('--n_samples', default=100000)
    parser.add_argument('--counts_file', default='counts.npy')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    add_arguments(parser)
    args = parser.parse_args()

    main(args)
