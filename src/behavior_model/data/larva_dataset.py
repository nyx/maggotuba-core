import torch
from torch.utils.data import Dataset
import numpy as np
import behavior_model.data.utils as utils
from behavior_model.data.enums import Feature
from behavior_model.data.enums import Label
import h5py



class LarvaDataset(Dataset):
    def __init__(self,
                dataset_file: str,
                n_segments: int,
                len_traj: int,
                len_pred: int,
                **kwargs):

        # calls make_dataset and initializes self.samples as a list of tuples (path_to_sample, label)
        super(LarvaDataset, self).__init__()

        self.dataset_file = dataset_file
        self.file_handle = h5py.File(self.dataset_file, 'r')
        self.n_segments = n_segments
        self.len_traj = len_traj
        self.len_pred = len_pred
        samples_dataset = self.file_handle['samples']
        try:
            assert(self.len_traj == samples_dataset.attrs['len_traj'])
            assert(self.len_pred == samples_dataset.attrs['len_pred'])
        except KeyError:
            print("cannot find 'len_traj' or 'len_pred' in 'samples' dataset")
            pass
        self.len = None
        len(self)

    def __getitem__(self, idx):
        '''Get an item from the dataset. If indices is specified, the item's index in the dataset is indices[index].'''

        if idx >= self.len:
            raise IndexError()

        f = self.file_handle

        path = f'samples/sample_{idx}'
        larva_name = f[path].attrs['path']
        data = f[path][...]

        present_label = Label[import_label(f[path].attrs['behavior'].upper())].value
        future_label = self._future_label(data)

        sample = utils.select_coordinates_columns(data)
        sample = utils.reshape(sample)
        sample = torch.from_numpy(sample).float()

        label = torch.Tensor([present_label, future_label]).float()

        metadata = {'present_label':present_label, 'future_label':future_label, 'larva_path':larva_name}

        return sample, metadata

    def __len__(self):
        if self.len is None:
            with h5py.File(self.dataset_file, 'r') as f:
                self.len = f['samples'].attrs['n_samples']
        return self.len

    def __del__(self):
        try:
            self.file_handle.close()
        except ImportError: # sys.meta_path is None, Python is likely shutting down
            pass

    def _future_label(self, data):
        # create future label, to use in plotting the next transition probability
        future = data[-self.len_pred:, Feature.FIRST_LABEL.value:Feature.LAST_LABEL.value+1]
        unique, count = np.unique(np.argmax(future, axis=1), return_counts=True)
        future_label = unique[np.argmax(count)]
        return future_label

def import_label(s):
    s = s.decode('utf-8') if isinstance(s, bytes) else s
    # MaggotUBA only understands BACK, BEND, HUNCH, ROLL, RUN, STOP.
    # With the `small_motion` branch, it also understands SMALL_MOTION.
    if s == 'CAST':
        return 'BEND'
    elif s == 'STOP_LARGE':
        return 'STOP'
    elif s == 'BACK_LARGE':
        return 'BACK'
    elif s == 'CAST_LARGE':
        return 'BEND'
    elif s == 'HUNCH_LARGE':
        return 'HUNCH'
    elif s == 'ROLL_LARGE':
        return 'ROLL'
    elif s == 'RUN_LARGE':
        return 'RUN'
    else:
        return s

