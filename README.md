This repository is a pruned and stable fork of an old version of MaggotUBA, whose source code has been released since then on [GitHub](https://github.com/DecBayComp/Detecting_subtle_behavioural_changes/tree/main/maggotuba).

<p align="center">
    <img src="maggotuba.svg" alt="drawing" width="400"/>
</p>

# MaggotUBA : Drosophila Larva Unsupervised Behavior Analysis

## Quickstart - using maggotuba command line tool

At the moment the pipeline is limited to the analysis of "Point dynamics" time series, which track the coordinates of 5 points along the longitudinal axis of the larva.

We assume your data is stored in a folder with the following structure :

```folder tree
raw_data_dir
├── t_5
│   ├── LINE_1
│   │   ├── protocol_1
│   │   │   ├── date_time1
│   │   │   │   ├── Point_dynamics_t5_LINE_1_protocol_1_larva_id_date_time1_larva_number_xx.txt
│   │   │   │   └──...
│   │   │   └──date_time2
│   │   │   └──date_time3
│   │   └──protocol_2
│   │   └──protocol_3
│   └──LINE_2
│   │   └──...
│   └──LINE_3
│           └──...
└──t_15
   └──mutatis mutandis
```

Most scripts support local multiprocessing. We recommend using a beefy desktop computer and setting the parameters `N` or `n_workers` to several dozens.

1. Create a project folder and `cd` into it.

```bash
maggotuba setup path/to/raw_data_dir path/to/project --len_traj LEN_TRAJ
cd path/to/project
```

2. Optionally, update parameters in `config.json`

3. Count the proportion of each behavior in the database :
 ```bash
 maggotuba db count -n_workers n_workers
 ```

4. Create a balanced database :
```bash
maggotuba db buil --n_workers n_workers
```

5. Train an autoencoder :
```bash
maggotuba model train --name experiment
```

6. Evaluate it :
```bash
maggotuba model eval --name experiment
```

7. ~Optionally, examine clusters in the latent space~:
```bash
maggotuba model cluster --name experiment
```

8. ~Compute the embeddings for the whole database~.
```bash
maggotuba model embed --name experiment --n_workers n_workers
```

9. ~Compute the MMD matrix for a particular tracker~
```bash
maggotuba model embed --name experiment --tracker t5 --n_workers n_workers
```

